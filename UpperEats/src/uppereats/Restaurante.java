package uppereats;

import java.io.Serializable;

public class Restaurante implements Serializable{
    private String nome;
    private String morada;
    private String localizacao;
    private Tipocozinha tipoCozinha;
    
    
    public Restaurante(String nome, String morada, String localizacao, Tipocozinha tipoCozinha){
        this.nome = nome;
        this.morada = morada;
        this.localizacao = localizacao;
        this.tipoCozinha = tipoCozinha;
    }

    public String getNome() {
        return nome;
    }

    public String getMorada() {
        return morada;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public Tipocozinha getTipoCozinha() {
        return tipoCozinha;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public void setTipoCozinha(Tipocozinha tipoCozinha) {
        this.tipoCozinha = tipoCozinha;
    }
}
