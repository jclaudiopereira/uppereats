package uppereats;

import java.io.*;
import java.util.*;

public class repositorio implements Serializable{
    
    
    public static repositorio bd = null;
    private static String filename = "UpperEats_data.dat";
    
    public repositorio(){
    
    
    }
    
    
    public static String getFilename() {
        return filename;
    }

    public static void setFilename(String filename) {
        repositorio.filename = filename;
    }
    
    
    public static void serializar(String filename) {
        // Serializar um objeto para ficheiro
        try (FileOutputStream fileOut = new FileOutputStream(filename);
                ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            out.writeObject(bd);
            out.close();
            fileOut.close();
            System.out.println("Serialized data is saved in " + filename);
        } catch (IOException ex) {
            System.out.println("Erro: " + ex.getMessage());
        }
    }

    public void desserializar(String filename) {
        try (FileInputStream fileIn = new FileInputStream(filename);
                ObjectInputStream in = new ObjectInputStream(fileIn);) {
            bd = (repositorio) in.readObject();
            in.close();
            fileIn.close();
            System.out.printf("Ficheiro " + filename + " lido com sucesso.");
        } catch (IOException ex) {
            System.out.println("Erro: " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("Cliente class not found. " + ex.getMessage());
        }
    }
    
}
